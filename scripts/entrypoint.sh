#!/bin/sh

case $PERIODIC in
	15min|daily|hourly|monthly|weekly)
		ln -sf /usr/local/bin/uacme.sh /etc/periodic/$PERIODIC/uacme;;
	*)  ln -sf /usr/local/bin/uacme.sh /etc/periodic/weekly/uacme;;
esac

uacme.sh

exec crond -fc /etc/crontabs
