#!/bin/sh

# available variables from uacme
METHOD=$1
TYPE=$2
AUTH=$5

write_conf() {
	cat <<- EOF
	server:
	    ip-address: 0.0.0.0
	    ip4-only: yes
	    hide-version: yes
	    identity: ""
	    zonesdir: "/etc/nsd"
	remote-control:
	    control-enable: yes
	zone:
	    name: $ZONE
	    zonefile: $ZONE.zone
	EOF
}

write_zone() {
	cat <<- EOF
	\$ORIGIN $ZONE.
	\$TTL 1m
	@ IN SOA ns-$ZONE. dns-admin.$ZONE. (
	    $(date +%y%m%d%H%M)			; serial
	    12h				; refresh
	    2h				; retry
	    2w				; expire
	    1h				; min TTL
	    )

	acme-${DOMAIN//./-}	1	IN	TXT	"$AUTH"
	
	EOF
}

# we are only interested in dns challenge
if [ "$TYPE" != "dns-01" ]; then
	echo "Skipping type: $TYPE"
	exit 1
fi

# create config and zone and start nsd
# when finished stop nsd again
case "$METHOD" in
	begin)
		echo "Writing nsd config and starting.."
		write_conf > /etc/nsd/nsd.conf
		write_zone > /etc/nsd/"$ZONE".zone
		nsd-control start
		nsd-control reload $ZONE ;;
	done)
		echo "ACME request successful.."
		nsd-control stop ;;
	failed)
		echo "ACME request failed!"
		nsd-control stop ;;
esac

