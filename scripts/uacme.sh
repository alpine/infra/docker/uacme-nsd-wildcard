#!/bin/sh

CONFDIR=/etc/ssl/uacme
HOOK=/usr/local/bin/uacme-hook.sh

[ "$STAGING" ] && opts="--staging --force --verbose"

case "$KEYTYPE" in
	RSA|EC) true;;
	*) KEYTYPE=EC;;
esac

if [ ! -f "/etc/nsd/nsd_control.pem" ]; then
	echo "Generating nsd certs/keys"
	nsd-control-setup
fi

if [ ! -f "$CONFDIR/private/key.pem" ]; then
	echo "Generating account key.."
	uacme $opts --yes --type="$KEYTYPE" new
fi

install_cert() {
	echo "Installing certificate for domain: $DOMAIN"
	install -Dm644 $CONFDIR/$DOMAIN/cert.pem \
		/var/www/localhost/htdocs/$DOMAIN/fullchain.pem
	( cd /var/www/localhost/htdocs/$DOMAIN
		sha512sum fullchain.pem > fullchain.pem.sha512 )
}

for DOMAIN in $DOMAINS; do
	echo "Generating certificates for \"$DOMAIN\""
	# needed variables in hook
	export DOMAIN CONFDIR
	if [ -f "$CONFDIR/private/$DOMAIN/key.pem" ]; then
		uacme $opts --never-create --hook="$HOOK" issue "$DOMAIN" "*.$DOMAIN" && install_cert
	else
		uacme $opts --hook="$HOOK" issue "$DOMAIN" "*.$DOMAIN" && install_cert
	fi
done

