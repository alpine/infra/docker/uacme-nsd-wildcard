FROM alpine

RUN apk --no-cache add nsd uacme darkhttpd openssl

COPY scripts/ /usr/local/bin/

EXPOSE 80 53/tcp 53/udp

ENTRYPOINT [ "entrypoint.sh" ]
